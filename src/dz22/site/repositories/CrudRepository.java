package dz22.site.repositories;

import java.util.List;
import java.util.Optional;

//CRUD -> creat, read, update,delete
public interface CrudRepository <T>{
    void save(T model);
    Optional<T>findById(Integer id);
    List<T> findAll();
}
