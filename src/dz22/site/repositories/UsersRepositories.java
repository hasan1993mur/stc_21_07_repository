package dz22.site.repositories;

import dz22.site.model.User;

import java.util.Optional;

public interface UsersRepositories extends CrudRepository<User> {
    Optional<User> findByAgeOrderAgeDesc(String age);
}
