package dz12.lecture_collections3.primitive;

public class Main {
    public static void main(String[] args) {
        Map<String, Integer> map = new HashMap<>();

        map.put("Хасан", 27);//коллизия с Романом
        map.put("Альберт", 22);//ОК
        map.put("Рамиль", 21);//коллизия с Андреем
        map.put("Антон", 23);//коллизия с Глебом
        map.put("Глеб", 24);//ОК
        map.put("Дамир", 25);//ОК
        map.put("Тагир", 26);// коллизия с Андреем
        map.put("Роман", 28);//ОК
        map.put("Андрей", 29);//ОК

        int i1 = map.get("Хасан");
        int i2 = map.get("Хасан");//ОК
        int i3 = map.get("4");
        int i4 = map.get("7");
        int i5 = map.get("Глеб");//ОК
        int i6 = map.get("Дамир");//ОК
        int i7 = map.get("Тагир");
        int i8 = map.get("Роман");//ОК
        int i10 = map.get("Андрей");//ОК


        System.out.println(i2);
//        System.out.println(i2);
//        System.out.println(i3);
//        System.out.println(i4);
//        System.out.println(i5);
//        System.out.println(i6);
//        System.out.println(i7);
//        System.out.println(i8);
//        System.out.println(i10);




    }
}
