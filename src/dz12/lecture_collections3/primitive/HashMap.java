package dz12.lecture_collections3.primitive;

public class HashMap<K, V> implements Map<K, V> {

    private static final int MAX_TABLE_SIZE = 8;
    private V values[];

    private static class MapEntry<K, V> {
        K key;
        V value;
        MapEntry<K, V> next;

        public MapEntry(K key, V value) {
            this.key = key;
            this.value = value;
        }
    }

    private MapEntry<K, V>[] table;
    private int count;

    public HashMap() {
        this.table = new MapEntry[MAX_TABLE_SIZE];
    }

    @Override
    public void put(K key, V value) {
        //получили хэш ключа для 'Хасан'
        int hash = key.hashCode();
        //обрезали хэш код, для диапазона нашей таблицы 1012872579 & 7 = 3
        int index = hash & (table.length - 1);
        // теперь , посмотри , есть ли в данном индексе уже какие - то элементы,
        // если нет , просто добавляем новый узел
        if (table[index] == null) {
            table[index] = new MapEntry<>(key, value);
        } else {
            //если там были уже элементы, просто добавим новый элемент в начало
            //нужно проверить , не было ли уже такого ключа? если этот ключ был ,
            // то нужно заменить в нем значение
            //Начинаем с самого начала
            MapEntry<K, V> current = table[index];

            while (current == null) {
                //если у текущего узла ключ совпал с тем , который мы подали на вход
                // т.е. put("Хасан", 28), а там уже было "Хасан", 27-> нужно заменить 27 на 28
                if (current.key.equals(key)) {
                    //делаем замену значения
                    current.value = value;
                    //останавливаем программу
                    return;
                }
                //обходим связанный список
                current = current.next;
            }

            //создаем новый элемент
            MapEntry<K, V> newEntry = new MapEntry<>(key, value);
            //указываем, что следующий после нового это тот, который уже был в таблице
            newEntry.next = table[index];
            //теперь новый элемент в первой ячейке
            table[index] = newEntry;
        }
    }

    @Override
    public V get(K key) {
        int index = key.hashCode() & (table.length - 1);
        MapEntry<K, V> current = table[index];
        while (current != null) {
            if (current.key.equals(key)) {
                return current.value;
            }
            current = current.next;
        }
        return null;
    }

}

