package dz12.lecture_collections3.primitive;

public interface Map <K,V>{
    void put(K key,V value);
    V get (K key);
}
