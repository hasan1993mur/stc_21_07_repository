package dz9.lecture;

public class ReversalNumberImpl implements NumbersProcess {
    @Override
    public int numbersProcess(int number) {
        int rev = 0;
        while (number != 0) {
            rev = rev * 10 + number % 10;
            number /= 10;
        }
        return rev;
    }
}
