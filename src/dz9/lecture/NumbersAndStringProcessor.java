package dz9.lecture;

public interface NumbersAndStringProcessor {
    int [] processInt(String [] processString);
    String [] processString (int [] processInt);
}
