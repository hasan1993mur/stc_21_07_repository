package dz9.lecture;

public class StringsProcess {

    private String text;
    private String rev;

    public void printProcess(){
        System.out.println(rev);
    }

    public String process(String text,StringProcess processString){
        rev = processString.process(text);
        return rev;
    }
}
