package dz9.lecture;

import java.util.Arrays;

public class NumbersAndStringProcessorImpl {
    private String[] processString;
    private int[] processInt;

    public void printProcess() {
        System.out.println("преобразование в массив строк- " + Arrays.toString(processString));
        System.out.println("преобразование в массив чисел- " + Arrays.toString(processInt));
    }

    public String[] process(int[] processInt, NumbersAndStringProcessor numbersAndStringProcessor) {
        processString = numbersAndStringProcessor.processString(processInt);
        return processString;
    }

    public int[] process(String[] processString, NumbersAndStringProcessor numbersAndStringProcessor) {
        processInt = numbersAndStringProcessor.processInt(processString);
        return processInt;
    }

    public NumbersAndStringProcessorImpl(String[] processString, int[] processInt) {
        this.processString = processString;
        this.processInt = processInt;
    }


}
