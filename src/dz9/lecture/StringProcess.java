package dz9.lecture;

public interface StringProcess {
    String process(String text);
}
