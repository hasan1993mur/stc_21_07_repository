package dz9.lecture;

import java.util.Arrays;

public class MainIntAndString {
    public static void main(String[] args) {
        String[] strings = {"1", "2", "3", "4", "5"};
        int[] ints = {1, 2, 3, 4, 5};

        NumbersAndStringProcessor numbersAndStringProcessor = new NumbersAndStringProcessor() {
            @Override
            public int[] processInt(String[] processString) {
                int[] array = Arrays.asList(strings).stream().mapToInt(Integer::parseInt).toArray();
                return array;
            }

            @Override
            public String[] processString(int[] processInt) {
                String[] array = Arrays.toString(ints).split("\\*");
                return array;
            }
        };

        NumbersAndStringProcessorImpl numbersAndStringProcessor1 = new NumbersAndStringProcessorImpl(strings,ints);
        numbersAndStringProcessor1.process(ints,numbersAndStringProcessor);
        numbersAndStringProcessor1.process(strings, numbersAndStringProcessor);

        numbersAndStringProcessor1.printProcess();
    }

}


