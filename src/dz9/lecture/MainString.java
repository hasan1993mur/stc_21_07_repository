package dz9.lecture;

public class MainString {
    public static void main(String[] args) {
        StringsProcess stringsProcess = new StringsProcess();
        StringProcess reversalText = text -> {
            String rev = new StringBuffer(text).reverse().toString();
            return rev;
        };
        stringsProcess.process("фыв313ыф", reversalText);
        stringsProcess.printProcess();

        StringProcess deleteNumber = new StringProcess() {
            @Override
            public String process(String text) {
                String rev = text.replaceAll("[0-9]+", "");
                return rev;
            }
        };
        stringsProcess.process("фыв313ыф", deleteNumber);
        stringsProcess.printProcess();
        StringProcess registreUp = new StringProcess() {
            @Override
            public String process(String text) {
                String rev = text.toUpperCase();
                return rev;
            }
        };
        stringsProcess.process("фыв313ыф" , registreUp);
        stringsProcess.printProcess();
    }
}
