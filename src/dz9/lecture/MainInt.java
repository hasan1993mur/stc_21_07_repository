package dz9.lecture;

public class MainInt {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();
        //  ReversalNumber reversalNumber= new ReversalNumberImpl();
        NumbersProcess revelsNumber = new NumbersProcess() {
            @Override
            public int numbersProcess(int number) {
                int rev = 0;
                while (number != 0) {
                    rev = rev * 10 + number % 10;
                    number /= 10;
                }
                return rev;
            }
        };
        numbersUtil.process(123, revelsNumber);
        numbersUtil.printProcess();

        NumbersProcess deleteNull = number -> {
            int rev = 0;
            while (number % 10 == 0) {
                number = number / 10;
                rev = number;
            }
            return rev;
        };
        numbersUtil.process(12300, deleteNull);
        numbersUtil.printProcess();

        NumbersProcess integerChange = new NumbersProcess() {
            @Override
            public int numbersProcess(int number) {
                int rev = 0;
                int place = 1;
                while (number > 0) {
                    if (number % 2 == 1) {
                        number -= 1;
                    }
                    rev = rev + ((number % 10) * place);
                    place *= 10;
                    number /= 10;
                }

                return rev;
            }
        };
        numbersUtil.process(3237, integerChange);
        numbersUtil.printProcess();


    }
}

