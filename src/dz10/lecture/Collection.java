package dz10.lecture;

public interface Collection {
    /**
     * добавляет элемент в коллекцию
     *
     * @param element добавляемый элемент
     */
    void add(int element);

    /**
     * проверяет , есть ли элемент в коллекции
     *
     * @param element искомый элемент
     * @return true , если элемет присутствует хотя бы один раз в коллекции
     */
    boolean contains(int element);

    /**
     * получает количество элементов в коллекции
     *
     * @return возвращает число элементов
     */
    int size();

    /**
     * удаляет первое вхождение элемента в список
     *
     * @param element удаляемый элемент
     */
    void remove(int element);

    /**
     * удаляет элемент по индексу
     * @param index удаляемый индекс
     */
    void removeByIndex(int index);


    Iterator1 iterator();
}
