package dz10.lecture;

public interface List extends Collection {
    /**
     * возвращает элемент под заданным индексом
     * @param index индекс элемента
     * @return элемент
     */
    int get(int index);

    void addFirst(int element);

    int indexOf(int element);
}
                