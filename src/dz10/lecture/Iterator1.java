package dz10.lecture;

public interface Iterator1 {
    /**
     * перейти к следующему элементу
     * @return элемент
     */
    int next();

    /**
     * проверить , есть ли следующий элемент
     * @return true, если следующий элемент есть, false если следующего элемента нет
     */
    boolean hasNext();
}
