package dz10.lecture;

public class LinkedList implements List {

    private static class Node {
        int value;
        Node next;

        public Node(int value) {
            this.value = value;
        }

    }

    //ссылка на начало списка, на первый узел
    private Node first;
    //ссылка на последний элемент списка, на последний узел
    private Node last;
    private int size;

    @Override
    public int get(int index) {
        if (index >= 0 && index < size) {
            Node current = first;

            for (int i = 0; i < index; i++) {
                //отчитываете index-штук узлов до нужного
                current = current.next;
            }
            return current.value;
        }
        return -1;
    }

    @Override
    public void addFirst(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            add(element);
        } else {
            //если уже есть элементы в списке
            //следующий узел после нового - это первый узел списка
            newNode.next = first;
            // теперь новый узел - первый в списке
            first = newNode;
        }
        size++;
    }

    @Override
    public void add(int element) {
        Node newNode = new Node(element);
        //если в списке еще нет элементов
        if (first == null) {
            //то первый и последний элемент - это новый
            first = newNode;
            last = newNode;
        } else {
            //теперь последний элемент списка ссылается на новый, следовательно новый встал в конец списка
            last.next = newNode;
            // теперь новый узел являегтся последним
            last = newNode;
        }
        size++;
    }

//    @Override
//    public void add(int element) {
//        Node newNode = new Node(element);
//        //если первый узел отсутствует
//        //то новый узел и есть первый
//        if (first == null) {
//            first = newNode;
//        }else {
//            //если узлы уже есть , то нужно дойти до последнего и добавить новй после него
//            Node current = first;
//            //идем по узлам , пока не дошли до последнего , последний узел , этот тот , у которого нет следующего
//            while (current.next != null){
//                current = current.next;
//            }
//            // делаем новый узел следующий после последнего
//            current.next = newNode;
//        }
//        size++;
//    }

    @Override
    public boolean contains(int element) {
        Node node = first;
        while (node != null) {
            if (node.value == element) {
                return true;
            } else {
                node = node.next;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }


    public int indexOf(int element) {
        int i = 0;
        Node current;

        for (current = first; current != null && current.value != element; current = current.next) {
            i++;
        }
        if (current != null) {
            return i;
        } else {
            return -1;
        }
    }

    @Override
    public void remove(int element) {
        removeByIndex(indexOf(element));
    }

    @Override
    public void removeByIndex(int index) {
        Node current = first;
        Node currentNext = current.next;

        if (index > 0 && index < size) {
            for (int i = 0; i < index - 1; i++) {
                current = current.next;
                currentNext = current.next;
            }
            current.next = currentNext.next;
            size--;
        } else if (index == 0) {
            first = current.next;
            size--;
        }
    }


    @Override
    public Iterator1 iterator() {
        return null;
    }

}
