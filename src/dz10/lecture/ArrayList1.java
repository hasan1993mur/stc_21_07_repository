package dz10.lecture;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

public class ArrayList1 implements List {
    private static final int DEFAULT_SIZE = 10;
    //ссылка на массив с элементами
    private int[] elements;
    private int size;
    private ArrayList<Integer> arrayList;


    public ArrayList1() {
        this.elements = new int[DEFAULT_SIZE];
        this.size = 0;
    }

    @Override
    public int get(int index) {
        if (index >= 0 && index < size) {
            return elements[index];
        } else {
            System.err.println("Out of bounds");
            return -1;
        }
    }

    @Override
    public void addFirst(int element) {
        if (size == elements.length) {
            resize();
        }
        //делаем сдвиг элементов массива на один в право
        for (int i = size + 1; i > 0; i--) {
            this.elements[i] = this.elements[i - 1];
        }
        this.elements[0] = element;
        size++;
    }

    @Override
    public int indexOf(int element) {
        for (int i = 0; i < size; i++) {
            if (elements[i] == element) {
                return i;
            }
        }
        return -1;

    }

    @Override
    public void add(int element) {
        //если элементов стало столько же , сколько в принцепе может вместиться в этот массив
        if (size == elements.length) {
            resize();
        }
        this.elements[size] = element;
        size++;
    }

    private void resize() {
        //создали массив, размер которого в полтора раза больше массива, в котором у нас сейчас лежат элементы
        int[] newElements = new int[elements.length + elements.length / 2];
        // копируем данные из массива с элементами в новый массив , больший по размеру
        for (int i = 0; i < size; i++) {
            newElements[i] = elements[i];
        }
        // изменяем ссылку на массив с элементами
        this.elements = newElements;
    }

    @Override
    public boolean contains(int element) {
        for (int i = 0; i < size; i++) {
            if (elements[i] == element) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int size() {
        return size;
    }


    //удаляем элемент из массива
    @Override
    public void remove(int element) {
        removeByIndex(indexOf(element));
    }

    public ArrayList<Integer> elementsAddArrayList() {
        int[] newElements = new int[elements.length];
        // копируем данные из массива с элементами в новый массив , больший по размеру
        for (int i = 0; i < size; i++) {
            newElements[i] = elements[i];
        }
        //инициализируем новую коллекцию
        arrayList = new ArrayList<>();
        for (int intValue : newElements) {
            // передаем значения масссива , в коллекцию
            arrayList.add(intValue);
        }
        return arrayList;
    }


    //удаление по индексу, спомощью коллекции
    @Override
    public void removeByIndex(int index) {
        if (index >= 0 && index < size){
            for (int i = index; i < size; i++){
                elements[index] = elements[index + 1];
                index++;
            }
        }
        size--;
    }

    private class ArrayListIterator implements Iterator1 {
        //текущая позиция итератора
        private int current = 0;

        @Override
        public int next() {
            // запомнили элемент, который хотим вернуть
            int element = elements[current];
            // сдвигаем позицию итератора
            current++;
            // возвращаем элемент
            return element;
        }

        @Override
        public boolean hasNext() {
            //пока не дошли до конца списка, возвращаем true
            return current < size;
        }
    }
    @Override
    public String toString() {
        return "ArrayList{" +
                "elements=" + Arrays.toString(elements) +
                ", size=" + size +
                '}';
    }


    @Override
    public Iterator1 iterator() {
        return new ArrayListIterator();
    }
}
