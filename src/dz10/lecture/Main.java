package dz10.lecture;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        LinkedList linkedList = new LinkedList();
        linkedList.add(2);
        Random random = new Random();
        for (int i = 0; i < 10; i++) {
            int a = random.nextInt(10);
            linkedList.add(a);
        }
        for (int i = 0; i < 10; i++) {
            System.out.println(linkedList.get(i));
        }

        System.out.println();
        System.out.println(linkedList.contains(2));
        System.out.println();

        linkedList.removeByIndex(2);
        for (int i = 0; i < 10; i++) {
            System.out.println(linkedList.get(i));
        }

    }
}
