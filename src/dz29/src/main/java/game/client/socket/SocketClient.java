package game.client.socket;


import game.client.controller.MainController;
import game.client.utils.GameUtil;
import javafx.application.Platform;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;


public class SocketClient extends Thread {
    private final Socket socket;
    private final PrintWriter toServer;
    private final BufferedReader fromServer;
    private final MainController controller;
    private final GameUtil gameUtils;
    private boolean isGameInProcess = true;

    public SocketClient(MainController controller, String host, int port) {
        try {
            this.socket = new Socket(host, port);
            this.toServer = new PrintWriter(socket.getOutputStream(), true);
            this.fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.controller = controller;
            this.gameUtils = controller.getGameUtils();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void run() {
        while (isGameInProcess) {
            String messageFromServer;

            try {
                messageFromServer = fromServer.readLine();

                if (messageFromServer != null) {
                    if (isStatistic(messageFromServer)){
                        statisticCommand(messageFromServer);
                    } else {
                        switchCommands(messageFromServer);
                    }
                }
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    private void statisticCommand(String messageFromServer) {
        gameUtils.setGameInProcess(false);
        isGameInProcess = false;
    }

    private void switchCommands(String messageFromServer) {
        switch (messageFromServer) {
            case "left":
                gameUtils.goRight(controller.getEnemy());
                break;
            case "right":
                gameUtils.goLeft(controller.getEnemy());
                break;
            case "shot":
                Platform.runLater(() -> gameUtils.createBulletFor(controller.getEnemy(), true));
                break;
        }
    }

    private boolean isStatistic(String message){
        return message.startsWith("Stat: ");
    }

    public void sendMessage(String message) {
        toServer.println(message);
    }


}
