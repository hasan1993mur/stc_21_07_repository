package game.client.utils;

import game.client.controller.MainController;
import game.client.socket.SocketClient;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class GameUtil {
    private static final int DAMAGE = 5;
    private static final int PLAYER_STEP = 5;
    private AnchorPane pane;
    private MainController mainController;
    private SocketClient socketClient;
    private final Lock lock = new ReentrantLock();
    private boolean isGameInProcess = true;


    public void goRight(ImageView player) {
        double position = player.getX() + PLAYER_STEP;

        if (position < 220) {
            player.setX(position);
        }
    }

    public void goLeft(ImageView player) {
        double position = player.getX() - PLAYER_STEP;

        if (position > -220) {
            player.setX(position);
        }
    }

    public ImageView createBulletFor(ImageView player, boolean isEnemy) {
        Image image = new Image("/images/image.png");
        ImageView bullet = new ImageView(image);
        pane.getChildren().add(bullet);
        bullet.setX(player.getX() + player.getLayoutX());
        bullet.setY(player.getY() + player.getLayoutY());

        int value = resolveValue(isEnemy);
        final ImageView target = resolveTarget(isEnemy);
        final Label targetHp = resolveTargetHp(isEnemy);

        Timeline timeLine = new Timeline(new KeyFrame(Duration.seconds(0.005), animation ->
                animationForBullet(isEnemy, bullet, value, target, targetHp)));
        timeLine.setCycleCount(500);
        timeLine.play();
        return bullet;
    }

    private void animationForBullet(boolean isEnemy, ImageView bullet, int value, ImageView target, Label targetHp) {
        bullet.setY(bullet.getY() + value);

        if (bullet.isVisible() && isIntersects(bullet, target)) {
            lock.lock();
            createDamage(targetHp);
            bullet.setVisible(false);
            lock.unlock();

            if (!isEnemy) {
                socketClient.sendMessage("Damage");
            }
        }

        if (isBulletOutside(bullet)) {
            pane.getChildren().remove(bullet);
        }
    }

    private int resolveValue(boolean isEnemy) {
        if (isEnemy) {
            return 1;
        } else {
            return -1;
        }
    }

    private ImageView resolveTarget(boolean isEnemy) {
        if (!isEnemy) {
            return mainController.getEnemy();
        } else {
            return mainController.getPlayer();
        }
    }

    private Label resolveTargetHp(boolean isEnemy) {
        if (!isEnemy) {
            return mainController.getEnemyHp();
        } else {
            return mainController.getPlayerHp();
        }
    }

    private void createDamage(Label targetHp) {
        int health = Integer.parseInt(targetHp.getText()) - DAMAGE;

        if (health >= 0) {
            targetHp.setText(String.valueOf(health));

            if (health == 0) {
                socketClient.sendMessage("exit");
            }
        }
    }

    private boolean isBulletOutside(ImageView bullet) {
        return bullet.getY() < 30 || bullet.getY() > 470;
    }

    private boolean isIntersects(ImageView bullet, ImageView target) {
        return bullet.getBoundsInParent().intersects(target.getBoundsInParent());
    }

    public void setPane(AnchorPane pane) {
        this.pane = pane;
    }

    public void setController(MainController mainController) {
        this.mainController = mainController;
    }

    public void setClient(SocketClient socketClient) {
        this.socketClient = socketClient;
    }

    public boolean getIsGameInProcess() {
        return isGameInProcess;
    }

    public void setGameInProcess(boolean gameInProcess) {
        isGameInProcess = gameInProcess;
    }

}

