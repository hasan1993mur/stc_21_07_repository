package game.client.controller;

import game.client.socket.SocketClient;
import game.client.utils.GameUtil;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;

import java.net.URL;
import java.util.ResourceBundle;


// контроллер осуществляет взаимодействие со сценой
public class MainController implements Initializable {

    private GameUtil gameUtils;

    private SocketClient socketClient;
    @FXML
    private ImageView player;

    @FXML
    private ImageView enemy;

    @FXML
    private Button buttonConnect;

    @FXML
    private Button buttonGo;

    @FXML
    private TextField playerName;

    @FXML
    private AnchorPane pane;

    @FXML
    private Label playerHp;

    @FXML
    private Label enemyHp;



    public EventHandler<KeyEvent> getKeyEventEventHandler() {
        return keyEventEventHandler;
    }

    private EventHandler<KeyEvent> keyEventEventHandler = event -> {
        if (gameUtils.getIsGameInProcess()) {
            if (event.getCode() == KeyCode.RIGHT) {
                gameUtils.goRight(player);
                socketClient.sendMessage("right");
            } else if (event.getCode() == KeyCode.LEFT) {
                gameUtils.goLeft(player);
                socketClient.sendMessage("left");
            } else if (event.getCode() == KeyCode.SPACE) {
                ImageView bullet = gameUtils.createBulletFor(player, false);
                socketClient.sendMessage("shot");
            }
        }
    };

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        gameUtils = new GameUtil();

        buttonConnect.setOnAction(event -> {
            socketClient = new SocketClient(this, "localhost", 7777);
            new Thread(socketClient).start();
            buttonConnect.setDisable(true);
            buttonGo.setDisable(false);
            playerName.setDisable(false);
            gameUtils.setPane(pane);
            gameUtils.setClient(socketClient);
        });

        buttonGo.setOnAction(event -> {
            socketClient.sendMessage("name: " + playerName.getText());
            buttonGo.setDisable(true);
            playerName.setDisable(true);
            buttonGo.getScene().getRoot().requestFocus();
        });

        gameUtils.setController(this);
    }

    public GameUtil getGameUtils() {
        return gameUtils;
    }

    public ImageView getEnemy() {
        return enemy;
    }

    public ImageView getPlayer() {
        return player;
    }

    public Label getPlayerHp() {
        return playerHp;
    }

    public Label getEnemyHp() {
        return enemyHp;
    }

}

