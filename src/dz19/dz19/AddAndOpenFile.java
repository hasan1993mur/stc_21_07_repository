package dz19;

import java.io.*;
import java.util.List;

public class AddAndOpenFile {

    private static String fileName;

    private static List<Auto>autos = GetAuto.autos();

    public static void addAndOpenFile(String fileName) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, false));
            bufferedWriter.write(autos + "\n");
            bufferedWriter.close();
        } catch (
                IOException e) {
            throw new IllegalArgumentException(e);
        }

        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            reader.lines()
                    .forEach(System.out::println);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
