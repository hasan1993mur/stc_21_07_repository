package dz19;

import java.util.ArrayList;
import java.util.List;

public class GetAuto {

    private static List<Auto> autos;

    private static int size;

    public static List<Auto> autos() {

        autos = new ArrayList<>();


        String camryAuto = "Камри";
        String golfAuto = "Гольф";
        String fordAuto = "Форд";

        String blackColor = "Черный";
        String yellowColor = "Желтый";
        String redColor = "Красный";


        Auto auto0 = new Auto(102, camryAuto, blackColor, 70000, 1200000);
        Auto auto1 = new Auto(103, golfAuto, yellowColor, 154649, 1234567);
        Auto auto2 = new Auto(104, fordAuto, redColor, 123456, 799999);
        Auto auto3 = new Auto(105, camryAuto, redColor, 0, 777777);
        Auto auto4 = new Auto(106, golfAuto, yellowColor, 1223, 700000);
        Auto auto5 = new Auto(107, fordAuto, yellowColor, 0, 800000);
        Auto auto6 = new Auto(108, camryAuto, yellowColor, 1000, 12000000);


        autos.add(auto0);
        autos.add(auto1);
        autos.add(auto2);
        autos.add(auto3);
        autos.add(auto4);
        autos.add(auto5);
        autos.add(auto6);

        return autos;
    }

    public int getSize(int size){
        size = autos.size();
        return size;
    }


}
