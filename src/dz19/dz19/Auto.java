package dz19;

import java.util.Objects;


public class Auto implements Comparable<Auto> {
    private Integer grs;
    private String model;
    private String color;
    private Integer mileage;
    private Integer price;

    private int count = 0;
    private int size;

    private GetAuto getAuto =new GetAuto();



    public int getCount() {
        count++;
        return count;
    }

    public Auto(Integer grs, String model, String color, Integer mileage, Integer price) {
        this.grs = grs;
        this.model = model;
        this.color = color;
        this.mileage = mileage;
        this.price = price;
    }


    public Integer getGrs() {
        return this.grs;
    }

    public String getModel() {
        return this.model;
    }

    public String getColor() {
        return this.color;
    }

    public Integer getMileage() {
        return this.mileage;
    }

    public Integer getPrice() {
        return this.price;
    }

    public String toString() {
        return this.grs + "|" + this.model + "|" + this.color + "|" + this.mileage + "|" + this.price + "|" + "\n";
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            Auto auto = (Auto) o;
            return Objects.equals(this.grs, auto.grs) && Objects.equals(this.model, auto.model) && Objects.equals(this.color, auto.color) && Objects.equals(this.mileage, auto.mileage) && Objects.equals(this.price, auto.price);
        } else {
            return false;
        }
    }

    public int hashCode() {
        return Objects.hash(new Object[]{this.grs, this.model, this.color, this.mileage, this.price});
    }

    @Override
    public int compareTo(Auto o) {
        return this.price - o.price;
    }
}
