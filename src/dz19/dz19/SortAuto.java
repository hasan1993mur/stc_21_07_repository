package dz19;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.function.Function;
import java.util.stream.Stream;

public class SortAuto {
    //передача URL(названия) файла
    private Path file = Paths.get("auto.txt");

    //разбор обьекта на массив
    private Function<String, Auto> function = line -> {
        String[] array = line.split("\\| ");
        return new Auto(Integer.parseInt(array[0]), array[1], array[2], Integer.parseInt(array[3]), Integer.parseInt(array[4]));
    };

    public void filterAuto() {
        try {
            //работа с файлом
            Files.lines(file)
                    .map(function)
                    .filter(auto -> auto.getColor().equals("Черный") || auto.getMileage() == 0)
                    .forEach(auto -> System.out.println("Номера всех автомобилей, имеющих черный " +
                            "цвет или нулевой пробег. = " + auto.getGrs()));

            Long countUniqueAuto = Files.lines(file)
                    .map(function)
                    .filter(auto -> auto.getPrice() >= 700_000 && auto.getPrice() <= 800_000)
                    .map(auto -> auto.getModel()).distinct().count();
            System.out.println("\nКоличество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. = " + countUniqueAuto);

            String colorMinPrice = Files.lines(file)
                    .map(function)
                    .min(Comparator.comparing(Auto::getPrice)).get().getColor();
            System.out.println("\nЦвет автомобиля с минимальной стоимостью. = " + colorMinPrice);

            Double averagePrice = Files.lines(file)
                    .map(function)
                    .filter(auto -> auto.getModel().equals("Камри"))
                    .mapToLong(auto -> auto.getPrice()).average().getAsDouble();
            System.out.println("\nСреднюю стоимость Toyota Camry. = " + averagePrice);


        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
