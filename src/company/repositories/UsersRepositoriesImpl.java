package company.repositories;

import company.util.IdGenerator;
import company.util.idGenerators;
import company.models.Users;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class UsersRepositoriesImpl implements UsersRepositories {

    private String fileName;
    private IdGenerator idGenerator;

    public UsersRepositoriesImpl(String fileName, IdGenerator idGenerator) {
        this.fileName = fileName;
        this.idGenerator = idGenerator;
    }

    @Override
    public void save(Users model) {
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileName, true));
            bufferedWriter.write(model.getIdGenerator() + "|" + model.getIpAddress() + "|" + model.getNameUsers() + "|" + model.getVictories() + "|" + model.getDefeats() + "\n");
            bufferedWriter.close();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update() {

    }

    @Override
    public void findByNickName() {

    }
}
