package company.repositories;

import company.models.Users;

public interface UsersRepositories {
    void save(Users model);
    void update();
    void findByNickName();
}
