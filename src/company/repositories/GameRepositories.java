package company.repositories;

public interface GameRepositories {
    void savingGameResults(int victories,int defeats);
    void findById();
    void update();

}
