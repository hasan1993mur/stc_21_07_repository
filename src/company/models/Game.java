package com.company.models;

import java.util.Objects;

public class Game {
    private String date;
    private String nameGamer;
    private int countShots;
    private double timeGame;

    public Game(String date, String nameGamer, int countShots, double timeGame) {
        this.date = date;
        this.nameGamer = nameGamer;
        this.countShots = countShots;
        this.timeGame = timeGame;
    }

    public String getDate() {
        return date;
    }

    public String getNameGamer() {
        return nameGamer;
    }

    public int getCountShots() {
        return countShots;
    }

    public double getTimeGame() {
        return timeGame;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Game game = (Game) o;
        return countShots == game.countShots &&
                Double.compare(game.timeGame, timeGame) == 0 &&
                Objects.equals(date, game.date) &&
                Objects.equals(nameGamer, game.nameGamer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, nameGamer, countShots, timeGame);
    }

    @Override
    public String toString() {
        return "Game{" +
                "date='" + date + '\'' +
                ", nameGamer='" + nameGamer + '\'' +
                ", countShots=" + countShots +
                ", timeGame=" + timeGame +
                '}';
    }
}
