package company.models;

import company.util.IdGenerator;

import java.util.Objects;

public class Users {

    private IdGenerator idGenerator;
    private String ipAddress;
    private String nameUsers;
    private final int MAX_POINTS_LIVE = 100;
    private int victories;
    private int defeats;

    public Users(String ipAddress, String nameUsers, int victories, int defeats) {
        this.ipAddress = ipAddress;
        this.nameUsers = nameUsers;
        this.victories = victories;
        this.defeats = defeats;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public String getNameUsers() {
        return nameUsers;
    }

    public int getMAX_POINTS_LIVE() {
        return MAX_POINTS_LIVE;
    }

    public int getVictories() {
        return victories;
    }

    public int getDefeats() {
        return defeats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Users users = (Users) o;
        return MAX_POINTS_LIVE == users.MAX_POINTS_LIVE &&
                victories == users.victories &&
                defeats == users.defeats &&
                Objects.equals(ipAddress, users.ipAddress) &&
                Objects.equals(nameUsers, users.nameUsers);
    }

    public void setIdGenerator(IdGenerator idGenerator) {
        this.idGenerator = idGenerator;
    }

    public IdGenerator getIdGenerator() {
        return idGenerator;
    }

    @Override
    public int hashCode() {
        return Objects.hash(ipAddress, nameUsers, MAX_POINTS_LIVE, victories, defeats);
    }

    @Override
    public String toString() {
        return "Users{" +
                "ipAddress='" + ipAddress + '\'' +
                ", nameUsers='" + nameUsers + '\'' +
                ", MAX_POINTS_LIVE=" + MAX_POINTS_LIVE +
                ", victories=" + victories +
                ", defeats=" + defeats +
                '}';
    }
}
