package com.company.models;

import java.util.Objects;

public class Shot {
    private double timeShot;
    private boolean hit;
    private int gameCount;
    private String whoShotWhom;

    public Shot(double timeShot, boolean hit, int gameCount, String whoShotWhom) {
        this.timeShot = timeShot;
        this.hit = hit;
        this.gameCount = gameCount;
        this.whoShotWhom = whoShotWhom;
    }

    public double getTimeShot() {
        return timeShot;
    }

    public boolean isHit() {
        return hit;
    }

    public int getGameCount() {
        return gameCount;
    }

    public String getWhoShotWhom() {
        return whoShotWhom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Shot shot = (Shot) o;
        return Double.compare(shot.timeShot, timeShot) == 0 &&
                hit == shot.hit &&
                gameCount == shot.gameCount &&
                Objects.equals(whoShotWhom, shot.whoShotWhom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timeShot, hit, gameCount, whoShotWhom);
    }

    @Override
    public String toString() {
        return "Shot{" +
                "timeShot=" + timeShot +
                ", hit=" + hit +
                ", gameCount=" + gameCount +
                ", whoShotWhom='" + whoShotWhom + '\'' +
                '}';
    }
}
