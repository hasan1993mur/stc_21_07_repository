package company.util;

public interface IdGenerator {
    Integer nextId();
}
