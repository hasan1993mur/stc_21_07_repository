package company.util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class IdGeneratorFileBased extends idGenerators implements IdGenerator {

    private String fileName;

    public IdGeneratorFileBased(String fileName) {
        this.fileName = fileName;
        try (Writer writer = new FileWriter(fileName, true)) {
            nextId();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Integer nextId() {
        Integer i = 1;
        return ++i;
    }
}
