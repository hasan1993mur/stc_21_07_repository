**Описание проекта**

_Package:_

`app`-папка с классом ```Main```
```Main```- класс для запуска сервера, с помощью технологии ```HikariConfig```;

`dataSource`- папка с классом ```SimpleDataSource```
```SimpleDataSource``` Класс для соединения postgresSQL с помощью интерфейса DataSource
метод `openConnection()`- открытие соединения с postgresSQL
метод `getConnection()` - получение соединения с postgresSQL

`models` - папка c классами ```Game , Player , Shot```
```Game``` - класс с переменными , для передачи в БД
```Player``` - класс с переменными , для передачи в БД
```Shot``` - - класс с переменными , для передачи в БД

`repositories` - папка с интерфейсами  ```GameRepositories , PlayerRepositories , ShotsRepositories```,
а так же классами ```GameRepositoriesImpl , PlayerRepositoriesImpl , ShotsRepositoriesImpl``` отвечающие за сохранения и обновления
значений в БД postgresSQL
метод `save(Game game)` класса ``GameRepositoriesImpl`` сохранение значений Game в БД postgresSQL
метод `getById(Long id)` класса ``GameRepositoriesImpl`` получение Player по id из БД postgresSQL
метод `update(Game game)` класса ``GameRepositoriesImpl`` обновление значений Game в БД postgresSQL
метод `save(Player playe)` класса ``PlayerRepositoriesImpl`` сохранение Player в БД postgresSQL
метод `findByNickname(String nickname)` класса ``PlayerRepositoriesImpl`` получение Player по nickname из БД postgresSQL
метод `update(Game game)` класса ``PlayerRepositoriesImpl`` обновление значений Player в БД postgresSQL
метод `save(Shot shot)` класса ``ShotsRepositories`` сохранение значений Shot БД postgresSQL

