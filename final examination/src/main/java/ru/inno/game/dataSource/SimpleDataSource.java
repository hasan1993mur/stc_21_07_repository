package ru.inno.game.dataSource;




import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

public class SimpleDataSource implements DataSource {

    private Connection connection;

    private String url;
    private String user;
    private String password;

    public SimpleDataSource(String url, String user, String password) {
        this.url = url;
        this.user = user;
        this.password = password;
    }

    private void openConnection() {
        try {
            this.connection = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {
            openConnection();
        }
        return connection;
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        throw new IllegalArgumentException();
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        throw new IllegalArgumentException();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        throw new IllegalArgumentException();
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        throw new IllegalArgumentException();
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        throw new IllegalArgumentException();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        throw new IllegalArgumentException();
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        throw new IllegalArgumentException();
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        throw new IllegalArgumentException();
    }
}
