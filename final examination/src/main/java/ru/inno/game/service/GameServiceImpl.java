package ru.inno.game.service;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;
import ru.inno.game.models.Shot;
import ru.inno.game.repositories.GamesRepositories;
import ru.inno.game.repositories.PlayersRepositories;
import ru.inno.game.repositories.ShotsRepositories;

import java.time.LocalDateTime;
import java.util.Optional;

public class GameServiceImpl implements GameService {

    private PlayersRepositories playersRepository;

    private GamesRepositories gamesRepository;

    private ShotsRepositories shotsRepository;

    public GameServiceImpl(PlayersRepositories playersRepository, GamesRepositories gamesRepository, ShotsRepositories shotsRepository) {
        this.playersRepository = playersRepository;
        this.gamesRepository = gamesRepository;
        this.shotsRepository = shotsRepository;
    }

    @Override
    public Integer startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname) {
        // получили первого игрока
        Player first = checkIxExists(firstIp, firstPlayerNickname);
        // получили второго игрока
        Player second = checkIxExists(secondIp, secondPlayerNickname);
        // создаем игру
        Game game = Game.builder()
                .playerFirst(first)
                .playerSecond(second)
                .secondsGameTimeAmount(0L)
                .playerFirstShotsCount(0)
                .playerSecondShotsCount(0)
                .dateTime(LocalDateTime.now())
                .build();
        gamesRepository.save(game);
        return game.getId();
    }

    @Override
    public void shot(Integer gameId, String shooterNickname, String targetNickname) {
        // получаем информацию об игроках
        Player shooter = playersRepository.findByNickname(shooterNickname).get();
        Player target = playersRepository.findByNickname(targetNickname).get();
        // находим игру
        Game game = gamesRepository.findById(gameId);
        // создаем выстрел
        Shot shot = Shot.builder()
                .shooter(shooter)
                .target(target)
                .dateTime(LocalDateTime.now())
                .game(game)
                .build();
        // увеличили очки у того, кто стрелял
        shooter.setPoints(shooter.getPoints() + 1);
        // если стрелял первый игрок
        if (game.getPlayerFirst().getId().equals(shooter.getId())) {
            // увеличиваем количество попаданий в этой игре
            game.setPlayerFirstShotsCount(game.getPlayerFirstShotsCount() + 1);
        }
        // если стрелял второй игрок
        if (game.getPlayerSecond().getId().equals(shooter.getId())) {
            game.setPlayerSecondShotsCount(game.getPlayerSecondShotsCount() + 1);
        }
        // обновляем данные по стреляющему
        playersRepository.update(shooter);
        // обновляем данные по игре
        gamesRepository.update(game);
        // сохраняем выстрел
        shotsRepository.save(shot);
    }

    @Override
    public void finishGame(Integer gameId, String winnerNickname, String loserNickname) {
        Game game = gamesRepository.findById(gameId);
        Player firstPlayer = playersRepository.findByNickname(winnerNickname).get();
        Player secondPlayer = playersRepository.findByNickname(loserNickname).get();

        if (firstPlayer.getNickname().equals(winnerNickname)) {
            //увеличиваем колличество побед у первого игрока
            firstPlayer.setWinsCount(firstPlayer.getWinsCount() + 1);
            //увеличиваем колличество побед у второго игрока
            secondPlayer.setLosesCount(secondPlayer.getLosesCount() + 1);
        } else if (firstPlayer.getNickname().equals(loserNickname)) {
            //увеличиваем колличество побед у первого игрока
            firstPlayer.setWinsCount(firstPlayer.getLosesCount() + 1);
            //увеличиваем колличество побед у второго игрока
            secondPlayer.setLosesCount(secondPlayer.getWinsCount() + 1);
        }
        //считаем разницу во время начала и окончания игры
        Long endGame = System.currentTimeMillis();
        Long startGame = game.getSecondsGameTimeAmount();
        game.setSecondsGameTimeAmount((endGame - startGame) / 1000);
        //обновляем данные
        playersRepository.update(firstPlayer);
        playersRepository.update(secondPlayer);
        gamesRepository.update(game);
    }

    // ищет игрока по никнейму, если такой игрок был - она меняет его IP
    // если игрока не было, она создает нового и сохраняет его
    private Player checkIxExists(String ip, String nickname) {
        Player result;

        Optional<Player> playerOptional = playersRepository.findByNickname(nickname);
        // если игрока под таким именем нет
        if (!playerOptional.isPresent()) {
            // создаем игрока
            Player player = Player.builder()
                    .ip(ip)
                    .nickname(nickname)
                    .losesCount(0)
                    .winsCount(0)
                    .points(0)
                    .build();
            // сохраняем его в репозитории
            playersRepository.save(player);
            result = player;
        } else {
            Player player = playerOptional.get();
            player.setIp(ip);
            playersRepository.update(player);
            result = player;
        }

        return result;
    }
}
