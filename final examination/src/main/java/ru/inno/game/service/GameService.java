package ru.inno.game.service;

public interface GameService {
    /**
     * метод вызывается для обеспечения начала игры. Если игрок с таким никнеймом уже есть , то мы работаем с ним
     * если нет - то создаем нового
     *
     * @param firstIp              - IP первого игрока
     * @param secondIp             - IP второго игрока
     * @param firstPlayerNickname  никнейм первого игрока
     * @param secondPlayerNickname никнейм второго игрока
     * @return возвращает индификаторы игры
     */
    Integer startGame(String firstIp, String secondIp, String firstPlayerNickname, String secondPlayerNickname);

    /**
     * фикцирует поподание игрока в противника
     *
     * @param gameId          индефикатор игры
     * @param shooterNickname кто стрелял
     * @param targetNickname  в кого попали
     */
    void shot(Integer gameId, String shooterNickname, String targetNickname);

    /**
     * фиксирует конец игры
     * @param gameId индефикатор игры
     * @param winnerNickname выигровший
     * @param loserNickname проигравший
     */
    void finishGame(Integer gameId, String winnerNickname, String loserNickname);
}
