package ru.inno.game.models;

import lombok.*;


@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode

public class Player {
    private Integer id;
    private String ip;
    private String nickname;
    private Integer points;
    private Integer winsCount;
    private Integer losesCount;
}
