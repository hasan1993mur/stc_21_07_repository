package ru.inno.game.models;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
@EqualsAndHashCode


public class Shot {
    private Long id;
    private LocalDateTime dateTime;
    private Game game;
    private Player shooter;
    private Player target;
}
