package ru.inno.game.app;


import ru.inno.game.repositories.*;
import ru.inno.game.service.GameService;
import ru.inno.game.service.GameServiceImpl;
import ru.inno.game.socket.GameServer;
import ru.inno.game.util.HikConfig;

public class Main {
    public static void main(String[] args) {
        HikConfig config = new HikConfig();

        PlayersRepositories playersRepositories = new PlayersRepositoriesImpl(config.dataSource());
        GamesRepositories gamesRepositories = new GamesRepositoriesImpl(config.dataSource());
        ShotsRepositories shotsRepositories = new ShotsRepositoriesImpl(config.dataSource());
        GameService gameService = new GameServiceImpl(playersRepositories, gamesRepositories, shotsRepositories);
        GameServer gameServer = new GameServer(gameService);
        gameServer.start(7777);
    }
}
