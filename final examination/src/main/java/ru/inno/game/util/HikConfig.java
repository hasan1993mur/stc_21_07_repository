package ru.inno.game.util;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class HikConfig {

    public HikariDataSource dataSource(){
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        hikariConfig.setDriverClassName("org.postgresql.Driver");
        hikariConfig.setUsername("postgres");
        hikariConfig.setPassword("hasan123");
        hikariConfig.setMaximumPoolSize(2);

        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        return dataSource;
    }
}
