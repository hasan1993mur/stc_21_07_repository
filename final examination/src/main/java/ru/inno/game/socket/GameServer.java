package ru.inno.game.socket;

import ru.inno.game.service.GameService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class GameServer {
    // поле, которое позволяет реализовать здесь серверное сокет-соединение
    private ServerSocket serverSocket;
    // два игрока нашего сервера
    //отдельный поток для первого игрока
    private ClientThread firstPlayer;
    //отдельный поток для второго игрока
    private ClientThread secondPlayer;

    //отдельный поток для первого игрока

    private static final Lock lock = new ReentrantLock();

    private String firstPlayerNickname;
    private String secondPlayerNickname;

    private GameService gameService;
    //игра в процессе
    private boolean gameInProcess = true;
    //флаг начала игры
    private boolean gameInStarted = false;
    //индефикатор игры
    private Integer gameId;


    public GameServer(GameService gameService) {
        this.gameService = gameService;
    }

    // запускаем наш серверный сокет на каком-то порту
    public void start(int port) {
        try {
            // создали объект серверного-сокета
            serverSocket = new ServerSocket(port);
            // он уводит текущий поток в ожидание, пока не подключится клиента
            // как только клиент подключится, он будет лежать в объектной переменной client
            System.out.println("SERVER: ожидаем подключения первого игрока");
            firstPlayer = connect();
            System.out.println("SERVER: первый игрок подключен");
            System.out.println("SERVER: ожидаем подключения второго игрока");
            secondPlayer = connect();
            System.out.println("SERVER: второй игрок подключен");
            // запустили побочный поток для работы со вторым клиентом
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    private ClientThread connect() {
        try {
            // уводит приложение в ожидание (wait) пока не присоединиться какой-либо клиент
            // как только клиент подключен к серверу
            // объект-соединение возвращается как результат выполнения метода
            Socket client = serverSocket.accept();
            // создали сокет-клиенту отдельный поток
            ClientThread playerThread = new ClientThread(client);
            // запустили этот поток
            playerThread.start();
            return playerThread;
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    // отдельный поток для клиента
    private class ClientThread extends Thread {

        private Socket client;
        // поток символов, которые мы отправляем клиенту
        private PrintWriter toClient;
        // поток символов, которые мы читаем от клиента
        private BufferedReader fromClient;

        public ClientThread(Socket client) {
            this.client = client;
            try {
                // обернули потоки байтов в потоки символов
                this.toClient = new PrintWriter(client.getOutputStream(), true);
                this.fromClient = new BufferedReader(new InputStreamReader(client.getInputStream()));
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        // мы в любое время можем получить сообщение от клиента
        // поэтому чтение сообщения от клиента должно происходить в побочном потоке
        @Override
        public void run() {
            while (gameInProcess) {
                String messageFromClient;
                try {
                    // прочитали сообщение от клиента
                    messageFromClient = fromClient.readLine();

                    if (messageFromClient != null) {
                        System.out.println("SERVER: получено сообщение <" + messageFromClient + ">");
                        if (isMessageForNickname(messageFromClient)) {
                            resolveNickname(messageFromClient);
                        } else if (isMessageForShot(messageFromClient)) {
                            resolveShot(messageFromClient);
                        } else if (isMessageForMove(messageFromClient)) {
                            resolveMove(messageFromClient);
                        } else if (isMessageForDamage(messageFromClient)) {
                            resolveDamage();
                        } else if (isMessageForStop(messageFromClient)) {
                            resolveStop(isMessageForStop(messageFromClient));
                        }

                        // игра должна начинаться только один раз и только в одном из потоков
                        // срабатывает, только если игра еще не началась
                        lock.lock();
                        if (isReadyForStart()) {
                            startGame();
                        }
                        lock.unlock();
                    }
                } catch (IOException e) {
                    throw new IllegalStateException();
                }
            }
        }

        private void resolveStop(boolean messageForStop) {
            gameInProcess = false;
            if (meFirst()) {
                gameService.finishGame(gameId, firstPlayerNickname, secondPlayerNickname);
            } else {
                gameService.finishGame(gameId, firstPlayerNickname, secondPlayerNickname);
            }
            try {
                sendMessage(firstPlayer, "STOP");
                sendMessage(secondPlayer, "STOP");
                toClient.close();
                fromClient.close();
                System.exit(0);
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }

        private boolean isMessageForStop(String messageFromClient) {
            return messageFromClient.equals("STOP");
        }

        private void resolveDamage() {
            if (meFirst()) {
                gameService.shot(gameId, firstPlayerNickname, secondPlayerNickname);
            } else {
                gameService.shot(gameId, secondPlayerNickname, firstPlayerNickname);
            }
        }

        private boolean isMessageForDamage(String messageFromClient) {
            return messageFromClient.equals("damage");
        }

        private void resolveMove(String messageFromClient) {
            // Отправить его другому клиенту
            // если вы - первый клиент
            if (meFirst()) {
                // отправляем сообщение второму
                sendMessage(secondPlayer, messageFromClient);
            } else {
                sendMessage(firstPlayer, messageFromClient);
            }
        }

        private boolean meFirst() {
            return this == firstPlayer;
        }

        private boolean isMessageForMove(String messageFromClient) {
            return messageFromClient.equals("left") || messageFromClient.equals("right");
        }

        private boolean isMessageForShot(String messageFromClient) {
            return messageFromClient.equals("shot");
        }

        private void resolveShot(String messageFromClient) {
            // Отправить его другому клиенту
            // если вы - первый клиент
            if (meFirst()) {
                // отправляем сообщение второму
                sendMessage(secondPlayer, messageFromClient);
            } else {
                sendMessage(firstPlayer, messageFromClient);
            }
        }

        private void resolveNickname(String messageFromClient) {
            String playerNickname = messageFromClient.split(" ")[1]; // playerNickname = Марсель
            // если это поток первого игрока
            if (meFirst()) {
                // запоминаем его никнейм
                firstPlayerNickname = playerNickname;
            } else {
                secondPlayerNickname = playerNickname;
            }
            // nickname Марсель
        }

        private boolean isMessageForNickname(String messageFromClient) {
            return messageFromClient.startsWith("nickname ");
        }

        private void startGame() {
            gameId = gameService.startGame(firstPlayer.client.getInetAddress().getHostAddress() + firstPlayer.client.getPort(),
                    secondPlayer.client.getInetAddress().getHostAddress() + secondPlayer.client.getPort(), firstPlayerNickname, secondPlayerNickname);
            gameInStarted = true;
        }

        private boolean isReadyForStart() {
            return firstPlayerNickname != null && secondPlayerNickname != null && !gameInStarted;
        }

        public void sendMessage(ClientThread player, String message) {
            player.toClient.println(message);
        }
    }
}
