package ru.inno.game.repositories;

import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.util.Optional;
import java.util.function.Function;

public class PlayersRepositoriesImpl implements PlayersRepositories {
    //language=SQL
    private final static String SQL_FIND_BY_NICKNAME = "select * from player where nickname = ?";

    //language=SQL
    private final static String SQL_SELECT = "select name from player where nickname = ?";

    //language=SQL
    private final static String SQL_INSERT = "insert into player(ip, nickname, points, max_wins_count, max_loses_count) values (?, ?, ?, ?, ?)";

    //language=SQL
    private final static String SQL_UPDATE = "update player set " + "ip = ?, nickname = ?, points = ?, "
            + "max_wins_count = ?, max_loses_count = ? where id=?";

    private DataSource dataSource;

    public PlayersRepositoriesImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    private final Function<ResultSet, Player> fullInfoPlayer = row -> {
        try {
            return Player.builder()
                    .id(row.getInt("id"))
                    .ip(row.getString("ip"))
                    .nickname(row.getString("nickname"))
                    .winsCount(row.getInt("max_wins_count"))
                    .losesCount(row.getInt("max_loses_count"))
                    .points(row.getInt("points"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    private final Function<ResultSet, Player> namePlayer = row -> {
        try {
            return Player.builder()
                    .nickname(row.getString("nickname"))
                    .build();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    };

    @Override
    public Optional<Player> findByNickname(String nickname) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_FIND_BY_NICKNAME)) {

            statement.setString(1, nickname);

            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    return Optional.of(Player.builder()
                            .id(result.getInt("id"))
                            .nickname(result.getString("nickname"))
                            .ip(result.getString("ip"))
                            .points(result.getInt("points"))
                            .build());
                } else {
                    return Optional.empty();
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    public Player getByNickName(String name) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(2, name);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Player player = namePlayer.apply(resultSet);

                return player;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void save(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            setPlayer(player, statement);

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert player");
            }

            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                player.setId(generatedKeys.getInt("id"));
            } else {
                throw new SQLException("Can't obtain id");
            }

        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Player player) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {

            setPlayer(player, statement);
            statement.setInt(6, player.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't update player");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private void setPlayer(Player player, PreparedStatement statement) throws SQLException {
        statement.setString(2, player.getNickname());
        statement.setString(1, player.getIp());
        statement.setInt(3, player.getPoints());
        statement.setInt(4, player.getWinsCount());
        statement.setInt(5, player.getLosesCount());
    }
}
