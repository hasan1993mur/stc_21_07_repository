package ru.inno.game.repositories;


import ru.inno.game.models.Shot;

public interface ShotsRepositories {
    void save(Shot shot);
}
