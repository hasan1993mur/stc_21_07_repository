package ru.inno.game.repositories;

import ru.inno.game.models.Game;
import ru.inno.game.models.Player;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;

public class GamesRepositoriesImpl implements GamesRepositories {


    //language=SQL
    private static final String SQL_INSERT = "insert into game(datetime, player_first, player_second, player_first_shots_count, player_second_shots_count, second_game_time_amount) values (?, ?, ?, ?, ?, ?)";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from game where id = ?";

    //language=SQL
    private final static String SQL_UPDATE = "update game set " + "dateTime = ?, " + "player_first = ?, " + "player_second = ?, " + "player_first_shots_count = ?, " +
            "player_second_shots_count = ?, " + "second_game_time_amount = ? " + "where id=?";

    private DataSource dataSource;

    public GamesRepositoriesImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Game game) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {

            setGame(game, statement);

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert game");
            }
            //получаю ключи , которые сгенерировала база
            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                //проставляю сгенерированный ключ, который называется id
                game.setId(generatedKeys.getInt("id"));
            } else {
                //если база не смогла вернуть сгенерированный ключ
                throw new SQLException("Can't obtain id");
            }
            generatedKeys.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Game getById(Integer gameId) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            statement.setLong(1, gameId);
            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    return Game.builder()
                            .id(result.getInt("id"))
                            .dateTime(LocalDateTime.parse(result.getString("datetime")))
                            .playerFirst(Player.builder().id(result.getInt("player_first")).build())
                            .playerSecond(Player.builder().id(result.getInt("player_second")).build())
                            .build();

                } else {
                    throw new SQLException("Empty result");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public void update(Game game) {
        // TODO: реализовать обновление информации об игре
        System.out.println("Произошло обновление игры " + game.getId());
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_UPDATE, Statement.RETURN_GENERATED_KEYS)) {
            setGame(game, statement);
            statement.setLong(7, game.getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert game");
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    @Override
    public Game findById(Integer id) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_SELECT_BY_ID)) {
            statement.setLong(1, id);
            try (ResultSet result = statement.executeQuery()) {
                if (result.next()) {
                    PlayersRepositories playersRepositories = new PlayersRepositoriesImpl(dataSource);
                    return Game.builder()
                            .id(result.getInt("id"))
                            .dateTime(LocalDateTime.parse(result.getString("datetime")))
                            .playerFirst(Player.builder().id(result.getInt("player_first")).build())
                            .playerSecond(Player.builder().id(result.getInt("player_second")).build())
                            .playerFirstShotsCount(result.getInt("player_first_shots_count"))
                            .playerSecondShotsCount(result.getInt("player_second_shots_count"))
                            .secondsGameTimeAmount(result.getLong("second_game_time_amount"))
                            .build();

                } else {
                    throw new SQLException("Empty result");
                }
            }
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

    private void setGame(Game game, PreparedStatement statement) throws SQLException {
        statement.setString(1, game.getDateTime().toString());
        statement.setLong(2, game.getPlayerFirst().getId());
        statement.setLong(3, game.getPlayerSecond().getId());
        statement.setInt(4, game.getPlayerFirstShotsCount());
        statement.setInt(5, game.getPlayerSecondShotsCount());
        statement.setLong(6, game.getSecondsGameTimeAmount());
    }

}
