package ru.inno.game.repositories;


import ru.inno.game.models.Game;

public interface GamesRepositories {
    void save(Game game);

    Game getById(Integer gameId);

    void update(Game game);

    Game findById(Integer id);
}
