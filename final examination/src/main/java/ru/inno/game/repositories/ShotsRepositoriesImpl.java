package ru.inno.game.repositories;

import ru.inno.game.models.Shot;

import javax.sql.DataSource;
import java.sql.*;

public class ShotsRepositoriesImpl implements ShotsRepositories {


    private DataSource dataSource;

    public ShotsRepositoriesImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    //language=SQL
    private static final String SQL_INSERT = "insert into shot(datetime, shooter, target, game_id) values (?, ?, ?, ?)";

    @Override
    public void save(Shot shot) {
        try (Connection connection = dataSource.getConnection();
             PreparedStatement statement = connection.prepareStatement(SQL_INSERT, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, shot.getDateTime().toString());
            statement.setLong(2, shot.getShooter().getId());
            statement.setLong(3, shot.getTarget().getId());
            statement.setLong(4, shot.getGame().getId());

            int affectedRows = statement.executeUpdate();

            if (affectedRows != 1) {
                throw new SQLException("Can't insert player");
            }
            //получаю ключи , которые сгенерировала база
            ResultSet generatedKeys = statement.getGeneratedKeys();

            if (generatedKeys.next()) {
                //проставляю сгенерированный ключ, который называется id
                shot.setId(generatedKeys.getLong("id"));
            } else {
                //если база не смогла вернуть сгенерированный ключ
                throw new SQLException("Can't obtain id");
            }
            generatedKeys.close();
        } catch (SQLException e) {
            throw new IllegalStateException(e);
        }
    }

}
