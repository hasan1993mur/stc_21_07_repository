package ru.inno.game.repositories;



import ru.inno.game.models.Player;

import java.util.Optional;

public interface PlayersRepositories {
    Optional<Player> findByNickname(String nickname);

    void save(Player player);
    // обновляет данные объекта по ВСЕМ ПОЛЯМ
    // player - {5, a, b, c}, player в БД - {5, x, y, c}
    // update(player) -> player в БД {5, a, b, c}
    void update(Player player);

}
